FROM node:18 AS builder

WORKDIR /usr/src/app

COPY package*.json ./
COPY prisma ./prisma/

RUN npm ci

COPY . .

RUN npm run build

FROM node:18

COPY package*.json ./
COPY --from=builder /usr/src/app/dist ./dist
COPY --from=builder /usr/src/app/dist ./prisma/
COPY prisma ./prisma/


RUN npm ci

EXPOSE 3000
CMD [ "npm", "run", "start:prod" ]