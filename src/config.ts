import * as process from "process";

export default () => ({
    openWeatherApiKey: process.env.OPEN_WEATHER_API_KEY,
})