import {BadRequestException, Injectable} from '@nestjs/common';
import {ConfigService} from "@nestjs/config";

@Injectable()
export class OpenWeatherService {
    constructor(private readonly configService: ConfigService) {}

    async getWeather(lat: number, lon: number, part: string) {
        const apiKey = this.configService.get('openWeatherApiKey')

        const response = await fetch(`https://api.openweathermap.org/data/3.0/onecall?lat=${lat}&lon=${lon}&exclude=minutely,hourly,${part}&appid=${apiKey}`, {
            method: "GET",
        });

        const json =  await response.json() as (WeatherData | {cod?: string, message?: string});

        if ("cod" in json && +json?.cod >= 400) throw new BadRequestException(json?.message)

        return json as WeatherData;
    }
}
