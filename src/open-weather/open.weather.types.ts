interface WeatherDescription {
    id: number;
    main: string;
    description: string;
    icon: string;
}

interface WeatherRain {
    '1h': number;
}

interface WeatherCurrent {
    dt: number;
    sunrise: number;
    sunset: number;
    temp: number;
    feels_like: number;
    pressure: number;
    humidity: number;
    dew_point: number;
    uvi: number;
    clouds: number;
    visibility: number;
    wind_speed: number;
    wind_deg: number;
    weather: WeatherDescription[];
    rain: WeatherRain;
}

interface WeatherMinutely {
    dt: number;
    precipitation: number;
}

interface WeatherHourly {
    dt: number;
    temp: number;
    feels_like: number;
    pressure: number;
    humidity: number;
    dew_point: number;
    uvi: number;
    clouds: number;
    visibility: number;
    wind_speed: number;
    wind_deg: number;
    wind_gust: number;
    weather: WeatherDescription[];
    pop: number;
}

interface WeatherDailyTemp {
    day: number;
    min: number;
    max: number;
    night: number;
    eve: number;
    morn: number;
}

interface WeatherDailyFeelsLike {
    day: number;
    night: number;
    eve: number;
    morn: number;
}

interface WeatherDaily {
    dt: number;
    sunrise: number;
    sunset: number;
    moonrise: number;
    moonset: number;
    moon_phase: number;
    temp: WeatherDailyTemp;
    feels_like: WeatherDailyFeelsLike;
    pressure: number;
    humidity: number;
    dew_point: number;
    wind_speed: number;
    wind_deg: number;
    weather: WeatherDescription[];
    clouds: number;
    pop: number;
    rain: number;
    uvi: number;
}

interface WeatherAlert {
    sender_name: string;
    event: string;
    start: number;
    end: number;
    description: string;
    tags: string[];
}

interface WeatherData {
    lat: number;
    lon: number;
    timezone_offset: number;
    current?: WeatherCurrent;
    daily?: WeatherDaily[];
    alerts?: WeatherAlert[];
}
