import {Module} from '@nestjs/common';
import {AppController} from './app.controller';
import {AppService} from './app.service';
import {WeatherInfoModule} from './weather-info/weather-info.module';
import {ConfigModule} from "@nestjs/config";
import config from "./config";

@Module({
    imports: [WeatherInfoModule, ConfigModule.forRoot({
        isGlobal: true,
        envFilePath: '.env',
        load: [config]
    })],
    controllers: [AppController],
    providers: [AppService],
})
export class AppModule {
}
