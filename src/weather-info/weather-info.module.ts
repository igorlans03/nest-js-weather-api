import { Module } from '@nestjs/common';
import { WeatherInfoService } from './weather-info.service';
import { WeatherInfoController } from './weather-info.controller';
import {PrismaService} from "../prisma.service";
import {OpenWeatherService} from "../open-weather/open-weather.service";

@Module({
  controllers: [WeatherInfoController],
  providers: [WeatherInfoService, PrismaService, OpenWeatherService],
})
export class WeatherInfoModule {}
