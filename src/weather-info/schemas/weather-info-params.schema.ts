import {z} from "zod";

export const weatherInfoParamsSchema = z.object({
    lat: z.union([z.string(), z.number()]).transform(Number),
    lon: z.union([z.string(), z.number()]).transform(Number),
    part: z.string()
        .transform(data => data.replaceAll(' ', ''))
        .refine(data => {
            const values = data.split(',');
            const acceptedValues = ['minutely', 'hourly'];
            return values.some(item => !acceptedValues.includes(item))
        }, 'According to tech notes i excluded some info from OpenWeather API, such as minutely, hourly as i wanted to keep the right format')
        .refine(data => {
            const values = data.split(',');
            const acceptedValues = ['daily', 'current', 'alerts', 'tags'];
            return values.some(item => acceptedValues.includes(item))
        }, 'Part is not in right format, please check OpenWeather docs (exclude param)')
})