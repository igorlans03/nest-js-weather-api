import {CallHandler, ExecutionContext, Injectable, NestInterceptor} from "@nestjs/common";
import {map, Observable} from "rxjs";
import {WeatherInfo} from "@prisma/client";


const filterObjectKeys = (keysToShow: string[], objectToChange: Record<string, any>) => {
    return Object.fromEntries(
        Object.entries(objectToChange)
            .filter(([key]) => keysToShow.includes(key))
    )
}

type WeatherInfoWithTypedJson = WeatherInfo & { info: WeatherData }

@Injectable()
export class FormatWeatherInterceptor implements NestInterceptor {

    format(data: WeatherInfoWithTypedJson) {
        const keysToShow = ['sunrise', 'sunset', 'temp', 'feels_like', 'pressure', 'humidity', 'uvi', 'wind_speed'];

        const weatherInfo = data.info;

        const current = weatherInfo?.current;
        const daily = weatherInfo?.daily;
        const alerts = weatherInfo?.alerts;

        const currentObj = current ? {
            current: filterObjectKeys(keysToShow, current)
        } : {}

        const alertsObj = alerts || {};

        const dailyObj = daily ? {
            daily: daily.map(item => {
                return filterObjectKeys(keysToShow, item)
            })
        } : {}


        return {...data, info: {...currentObj, ...dailyObj, ...alertsObj}};
    }

    intercept(context: ExecutionContext, next: CallHandler<WeatherInfo | WeatherInfo[]>): Observable<any> | Promise<Observable<any>> {
        return next.handle().pipe(map((data: WeatherInfoWithTypedJson | WeatherInfoWithTypedJson[]) => {
            if (Array.isArray(data)) {
                return data.map(item => this.format(item))
            }

            return this.format(data)
        }))
    }
}