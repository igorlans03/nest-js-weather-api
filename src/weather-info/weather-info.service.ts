import {BadRequestException, Injectable, Logger} from '@nestjs/common';
import {WeatherInfoParamsDto} from "./dto/weather-info-params.dto";
import {PrismaService} from "../prisma.service";
import {OpenWeatherService} from "../open-weather/open-weather.service";

@Injectable()
export class WeatherInfoService {
    private logger = new Logger(WeatherInfoService.name);

    constructor(
        private readonly prisma: PrismaService,
        private readonly openWeather: OpenWeatherService
    ) {
    }


    async getByParams(dto: WeatherInfoParamsDto) {
        return this.prisma.weatherInfo.findUnique({
            where: {
                lat_lon_part: {
                    lat: dto.lat,
                    lon: dto.lon,
                    part: dto.part
                }
            }
        })
    }

    async getAll() {
        return this.prisma.weatherInfo.findMany();
    }

    async create(dto: WeatherInfoParamsDto) {
        const dbWeatherInfo = await this.getByParams(dto);

        if (dbWeatherInfo) throw new BadRequestException("Weather info with such params is already exist")

        const weather = await this.openWeather.getWeather(dto.lat, dto.lon, dto.part)

        return this.prisma.weatherInfo.create({
            data: {
                lat: dto.lat,
                lon: dto.lon,
                part: dto.part,
                info: weather as any
            }
        })

    }
}
