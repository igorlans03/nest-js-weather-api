import {
    Body,
    Controller,
    Get,
    NotFoundException,
    Post,
    Query, UseInterceptors,
} from '@nestjs/common';
import {WeatherInfoService} from './weather-info.service';
import {WeatherInfoParamsDto} from "./dto/weather-info-params.dto";
import {FormatWeatherInterceptor} from "./interceptors/format-weather.interceptor";

@Controller('weather')
export class WeatherInfoController {
    constructor(private readonly weatherInfoService: WeatherInfoService) {
    }

    @Get('get-all')
    @UseInterceptors(FormatWeatherInterceptor)
    getAll() {
        return this.weatherInfoService.getAll();
    }

    @Get('get-one')
    @UseInterceptors(FormatWeatherInterceptor)
    async getByParams(@Query() dto: WeatherInfoParamsDto) {
        const dbWeatherInfo = await this.weatherInfoService.getByParams(dto);

        if (!dbWeatherInfo) throw new NotFoundException("Weather info is not found");

        return dbWeatherInfo
    }

    @Post('create')
    create(@Body() dto: WeatherInfoParamsDto) {
        return this.weatherInfoService.create(dto)
    }
}
