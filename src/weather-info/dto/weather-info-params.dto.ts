import {createZodDto} from "nestjs-zod";
import {weatherInfoParamsSchema} from "../schemas/weather-info-params.schema";

export class WeatherInfoParamsDto extends createZodDto(weatherInfoParamsSchema) {}